import React, {useState} from 'react';

import 'react-dropzone-uploader/dist/styles.css';
import Dropzone from 'react-dropzone-uploader';
import styles from './styles.module.css';
import {Grid, Typography} from "@material-ui/core";
import PublishIcon from '@material-ui/icons/Publish';
import ImageLoader from 'react-image-file';
import cartoonService from "../../services/cartoon-service";

import ReactGA from "react-ga";
const handleChangeStatus = ({ meta, file }, status) => { console.log(status, meta, file) }


function FileReceptor() {
  const [cartoon, setCartoon] = useState({file: 'https://static.thenounproject.com/png/574704-200.png'});

  const handleSubmit = async (files, allFiles) => {
    const uniqueFile = files[0].file; // forgive me father
  ReactGA.event({
    category: 'Usuario',
    action: 'Imagen Enviada'
  }); 

    try {
      const res = await cartoonService.sendImageToConvert(uniqueFile);
      const { data: { responses_docs: docs } } = res;
      const { file_name: filename } = docs[0]; // forgive again

      const { data } = await cartoonService.getCartoonImage(filename);
      setCartoon({ file: data });

    } catch (e) {
      console.log('error enviando imagen para convertir a cartoon');
      console.error(e);
    }
  }

  return (
    <div className={styles.containerDropzone}>
      <Dropzone
        onChangeStatus={handleChangeStatus}
        onSubmit={handleSubmit}
        accept=".jpg,.jpeg"
        styles={{
          dropzone: { width: '100%', height: 200, overflow: 'hidden', backgroundColor: '#ECECEC' },
          inputLabel: { color: '#707070' },
          submitButton: { backgroundColor: '#29985E', borderRadius: '0px' },
          dropzoneReject: { borderColor: '#F2DEDE', backgroundColor: '#F2DEDE' },
          dropzoneActive: { borderColor: '#29985E' },
        }}
        inputContent={(
          <Grid key="1" container direction="column" justify="center" alignItems="center">
            <Grid item xs={6}>
              <PublishIcon style={{ fontSize: 80, color: '#000000' }} />
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6"> Arrastra hasta aquí o selecciona una imagen </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body2">Formatos permitidos (.jpg, .jpeg)</Typography>
            </Grid>
          </Grid>
        )}
        submitButtonContent="Convertir a cartoon"
        maxFiles={1}
      />

      <img src={typeof cartoon.file === 'string' ? cartoon.file : URL.createObjectURL(cartoon.file)} alt="cartoon" className={styles.cartoonImage}/>
    </div>
  );
}

export default FileReceptor;
