import React, {useEffect} from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './styles.module.css';
import FileReceptor from "../components/FileReceptor";
import ReactGA from "react-ga";


function Home() {
  useEffect(()=>{
		ReactGA.initialize('UA-186288506-1')
	}, [])
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
      </header>

      <main className={clsx(styles.fileReceptor)}>
        <FileReceptor />
      </main>
    </Layout>
  );
}

export default Home;
